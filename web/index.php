<style>
body {
    background: #ffffff url("BoilermakerSpecial.jpeg") no-repeat right top;
    margin-right: 400px;
}
p.groove {
	border-style: groove;
	border-width: 10px;
	border-color: blue;
}
</style>
<?php

require('../vendor/autoload.php');

$app = new Silex\Application();
$app['debug'] = true;

// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

// Register the Twig templating engine
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path' => __DIR__.'/../views',
));

//Database connection
$dbopts = parse_url(getenv('DATABASE_URL'));
$app->register(new Herrera\Pdo\PdoServiceProvider(),
  array(
    'pdo.dsn' => 'pgsql:dbname='.ltrim($dbopts["path"],'/').';host='.$dbopts["host"],
    'pdo.port' => $dbopts["port"],
    'pdo.username' => $dbopts["user"],
    'pdo.password' => $dbopts["pass"]
  )
);

// Our web handlers

$app->get('/db/', function() use($app) {
  $st = $app['pdo']->prepare('SELECT name FROM test_table');
  $st->execute();

  $names = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $app['monolog']->addDebug('Row ' . $row['name']);
    $names[] = $row;
  }

  return $app['twig']->render('database.twig', array(
    'names' => $names
  ));
});

$app->get('/twig/{name}', function ($name) use ($app) {
  $app['monolog']->addDebug('logging output in twig part BQL.');
    return $app['twig']->render('index.twig', array(
        'name' => $name,
    ));
});

$app->get('/', function() use($app) {
  $app['monolog']->addDebug('logging output in get section Hello BQL.');
  return '<p class="groove">ACS567 Project 1 - Heroku PHP</p>';
});

$app->get('/repeat', function() use($app) {
  $app['monolog']->addDebug('logging output in get repeate section Hello BQL.');
  return str_repeat('<p class="groove">ACS567 Project 1 - Heroku PHP</p>', getenv('TIMES'));
});

$app->run();

?>
